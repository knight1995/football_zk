package com.duy.dao;

import com.duy.entity.Players;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by 1812n on 7/14/2017.
 */
@Repository
public class PlayerDAOImpl extends AbstractDao<Integer,Players> implements PlayerDAO {
    @SuppressWarnings("unchecked")
    public List<Players> findAll() {
        transactionalSession();
        Criteria criteria = createEntityCriteria();
        return (List<Players>) criteria.list();
    }
    @Autowired
    private SessionFactory sessionFactory;

    protected Session transactionalSession() {
        return sessionFactory.getCurrentSession();
    }
}
