package com.duy.dao;

import com.duy.entity.Players;

import java.util.List;

/**
 * Created by 1812n on 7/11/2017.
 */
public interface PlayerDAO  {

    List<Players> findAll();

}
