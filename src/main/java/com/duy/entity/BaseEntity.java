package com.duy.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by 1812n on 7/3/2017.
 */
@MappedSuperclass
public abstract class BaseEntity implements Serializable{
    private transient String colId = "ID";
    private transient String colName = "NAME";
    private transient String[] uniqueColumn = new String[0];
    private transient static HashMap<String, Date> hmLastUpdateBoTimes = new HashMap<String, Date>();

    public String getBOName() {
        return this.getClass().getSimpleName();
    }

    public String getColId() {
        return colId;
    }
    public void setColId(String colId) {
        this.colId = colId;
    }
}
