package com.duy.entity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by 1812n on 7/3/2017.
 */
@Entity
@Table(name = "seasons")
public class Seasons extends BaseEntity implements Serializable{
    @Id
    @Column(name = "id", nullable = false, length = 25)
    private Long id;

    @Column(name = "name", nullable = false)
    private String name;

    @ManyToOne
    @JoinColumn(name = "leagues_id")
    private Leagues leagues;

    public Seasons() {
    }

    public Seasons(Long id, String name, Leagues leagues) {
        this.id = id;
        this.name = name;
        this.leagues = leagues;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Leagues getLeagues() {
        return leagues;
    }

    public void setLeagues(Leagues leagues) {
        this.leagues = leagues;
    }
}
