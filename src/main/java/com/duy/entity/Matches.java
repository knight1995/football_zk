package com.duy.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by 1812n on 7/3/2017.
 */
@Entity
@Table(name = "matches")
public class Matches extends BaseEntity implements Serializable {
    @Id
    @Column(name = "id",nullable = false,length = 25)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "home_team_id")
    private Teams team1;

    @ManyToOne
    @JoinColumn(name = "away_team_id")
    private Teams team2;

    @Column(name = "start_time")
    private Date start_time;

    @Column(name = "home_team_score")
    private Integer home_team_score;

    @Column(name = "away_team_score")
    private Integer away_team_score;

    public Matches() {
    }

    public Matches(Long id, Teams team1, Teams team2, Date start_time, Integer home_team_score, Integer away_team_score) {
        this.id = id;
        this.team1 = team1;
        this.team2 = team2;
        this.start_time = start_time;
        this.home_team_score = home_team_score;
        this.away_team_score = away_team_score;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Teams getTeam1() {
        return team1;
    }

    public void setTeam1(Teams team1) {
        this.team1 = team1;
    }

    public Teams getTeam2() {
        return team2;
    }

    public void setTeam2(Teams team2) {
        this.team2 = team2;
    }

    public Date getStart_time() {
        return start_time;
    }

    public void setStart_time(Date start_time) {
        this.start_time = start_time;
    }

    public Integer getHome_team_score() {
        return home_team_score;
    }

    public void setHome_team_score(Integer home_team_score) {
        this.home_team_score = home_team_score;
    }

    public Integer getAway_team_score() {
        return away_team_score;
    }

    public void setAway_team_score(Integer away_team_score) {
        this.away_team_score = away_team_score;
    }
}
