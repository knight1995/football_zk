package com.duy.entity;

import javax.persistence.*;
import javax.swing.text.Position;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by 1812n on 7/3/2017.
 */
@Entity
@Table(name = "players")
public class Players implements Serializable{
    @Id
    @Column(name = "id", nullable = false,length = 25)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "name",nullable = false)
    private String name;

    @Column(name = "age")
    private Integer age;

    @Column(name = "height")
    private Integer height;

    @Column(name = "weight")
    private Integer weight;

    @Column(name = "joined")
    private Date joined;

    @Column(name = "position")
    private Position position;

    @ManyToOne
    @JoinColumn(name = "team_id")
    private Long team_id;

    public Players() {
    }

    public Players(Long id, String name, Integer age, Integer height, Integer weight, Date joined, Position position, Long team_id) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.height = height;
        this.weight = weight;
        this.joined = joined;
        this.position = position;
        this.team_id = team_id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    public Date getJoined() {
        return joined;
    }

    public void setJoined(Date joined) {
        this.joined = joined;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public Long getTeam_id() {
        return team_id;
    }

    public void setTeam_id(Long team_id) {
        this.team_id = team_id;
    }
}
