package com.duy.entity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by 1812n on 7/3/2017.
 */
@Entity
@Table(name = "teams")
public class Teams extends BaseEntity implements Serializable{
    @Id
    @Column(name = "id", nullable = false, length = 25)
    private Long id;

    @Column(name = "name",nullable = false)
    private String name;

    @OneToMany
    @JoinColumn(name = "captain_id")
    private Teams teams;

    public Teams() {
    }

    public Teams(Long id, String name, Teams teams) {
        this.id = id;
        this.name = name;
        this.teams = teams;
    }

    public Long getId() {
        return id;
    }


    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Teams getTeams() {
        return teams;
    }

    public void setTeams(Teams teams) {
        this.teams = teams;
    }
}
