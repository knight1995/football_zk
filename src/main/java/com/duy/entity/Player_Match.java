package com.duy.entity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by 1812n on 7/3/2017.
 */
@Entity
@Table(name = "player_match")
public class Player_Match extends BaseEntity implements Serializable {
    @Id
    @Column(name = "id",nullable = false,length = 25)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "match_id")
    private Matches matches;

    @ManyToOne
    @JoinColumn(name = "player_id")
    private Players players;

    @Column(name = "yellow_card")
    private Integer yellow_card;

    @Column(name = "red_card")
    private Integer red_card;

    @Column(name = "goal_number")
    private Integer goal_number;

    @Column(name = "own_goal_number")
    private Integer own_goal_number;
}
