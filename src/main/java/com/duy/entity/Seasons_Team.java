package com.duy.entity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by 1812n on 7/3/2017.
 */
@Entity
@Table(name = "seasons_team")
public class Seasons_Team extends BaseEntity implements Serializable{
    @OneToMany
    @JoinColumn(name = "team_id")
    private Teams teams;

    @OneToMany
    @JoinColumn(name = "seasons_id")
    private Seasons seasons;

    public Seasons_Team() {
    }

    public Seasons_Team(Teams teams, Seasons seasons) {
        this.teams = teams;
        this.seasons = seasons;
    }

    public Teams getTeams() {
        return teams;
    }

    public void setTeams(Teams teams) {
        this.teams = teams;
    }

    public Seasons getSeasons() {
        return seasons;
    }

    public void setSeasons(Seasons seasons) {
        this.seasons = seasons;
    }
}
