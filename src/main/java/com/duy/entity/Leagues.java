package com.duy.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * Created by 1812n on 7/3/2017.
 */
@Entity
@Table(name = "leagues")
public class Leagues extends BaseEntity implements Serializable {
    @Id
    @Column(name = "id", nullable = false, length = 25)
    private Long id;

    @Column(name = "name", nullable = false)
    private  String name;

    public Leagues() {
    }

    public Leagues(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
