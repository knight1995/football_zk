package com.duy.service.iplm;

import com.duy.dao.PlayerDAO;
import com.duy.entity.Players;
import com.duy.service.PlayersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by 1812n on 7/13/2017.
 */
@Service
@Transactional
public class PlayerServiceImpl implements PlayersService {
    @Autowired
    private PlayerDAO playerDAO;

    public List<Players> getAll() {
        return this.playerDAO.findAll();
    }
    public List<Players> getList(Players players) {
        return this.playerDAO.findAll();
    }

    public PlayerDAO getPlayerDAO() {
        return playerDAO;
    }

    public void setPlayerDAO(PlayerDAO playerDAO) {
        this.playerDAO = playerDAO;
    }
}
