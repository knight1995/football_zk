package com.duy.service;

import com.duy.entity.Players;

import java.util.List;

/**
 * Created by 1812n on 7/7/2017.
 */
public interface PlayersService {


    List<Players> getAll();

    List<Players> getList(Players players);

}
