package com.duy.composer;

import com.duy.entity.Players;
import com.duy.service.PlayersService;
import com.duy.service.iplm.PlayerServiceImpl;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zul.*;

import java.util.List;


/**
 * Created by 1812n on 7/11/2017.
 */
@Controller
@RequestMapping("/")
public class playerComposer extends SelectorComposer<Component>{
    private Grid gridSearch;
    private Listbox lbxDsCauThu;
    private Button btnTK;
    private Button btnThem;
    private Button btnSua;
    private Button btnXoa;
    private Textbox txtID;
    private Textbox txtName;
    private Textbox txtAge;
    private List<Players> listResuilt;
    private PlayersService playersService = new PlayerServiceImpl() ;

    @Override
    public void doAfterCompose(Component comp) throws Exception{
        super.doAfterCompose(comp);
        bindData();
        bindGridData();
    }

    private void bindGridData() throws Exception{
        Players players=getDataSearchFromClient();
        List<Players> playersList= playersService.getList(players);
        if (playersList !=null && !listResuilt.isEmpty()){
            ListModelList modelList= new ListModelList(listResuilt);
            modelList.setMultiple(true);
            lbxDsCauThu.setModel(modelList);

        }else {
            lbxDsCauThu.setModel(new ListModelList());
        }
    }

    private void bindData() {
        List<Players> playersList = playersService.getAll();
        if(playersList !=null && playersList.size()>0){
            ListModelList modelList= new ListModelList(playersList);
            modelList.setMultiple(true);
            lbxDsCauThu.setModel(modelList);
        }else{
            lbxDsCauThu.setModel(new ListModelList() {
            });
        }
    }


    public Players getDataSearchFromClient() {
        Players players= new Players();
        if(txtName.getValue()== null || txtName.equals(txtName.toString().trim())){
            players.setName(txtName.getValue().toString().trim());
        }
        return players;
    }
}
